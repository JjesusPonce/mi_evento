package com.example.fiber.mievento;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Toast;

import java.io.File;
import java.util.Calendar;
import java.util.regex.Pattern;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.media.MediaScannerConnection.OnScanCompletedListener;

public class RegistroUsuarioFinal extends AppCompatActivity implements View.OnClickListener {
    Button Cancelar;
    Button Calendario;
    Button Crear;

    private static String APP_DIRECTORY ="Mypictureapp/";
    private static String MEDIA_DIRECTORY = APP_DIRECTORY+ "Pictureapp";
    private final int MY_PERMISSIONS = 100;
    private final int PHOTO_CODE =200;
    private final int SELECT_PICTURE = 300;

    private ImageView imagenfotouser;
    private Button botonañadir;
    private ScrollView scrolluser;
    private EditText nombpers;
    private EditText apepers;
    private EditText usuario;
    private EditText password;
    private EditText email;
    private EditText Fecha;
    private EditText CodArea;
    private EditText Telefono;
    private RadioGroup sexo;
    private boolean nombreperapp;
    private boolean apelidoperapp;
    private boolean usuarioapp;
    private boolean passapp;
    private boolean emailapp;
    private boolean fechaapp;
    private boolean codareaapp;
    private boolean telefonoapp;
    private TextInputLayout nombrepersona;
    private TextInputLayout apellidopersona;
    private TextInputLayout usuariomovil;
    private TextInputLayout passmovil;
    private TextInputLayout emailmovil;
    private TextInputLayout Fechamovil;
    private TextInputLayout CodAreamovil;
    private TextInputLayout Telefonomovil;

    private String mPath;

    private int Dia,Mes,Año;


    @Override
    public void onBackPressed() {
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_usuario_final);

        Crear = (Button)findViewById(R.id.BtnCrear);
        imagenfotouser=(ImageView)findViewById(R.id.Ivfotouser);
        botonañadir = (Button)findViewById(R.id.BtnAñadir);
        scrolluser= (ScrollView)findViewById(R.id.Scrolluser);
        nombpers= (EditText) findViewById(R.id.EtNombre);
        apepers= (EditText)findViewById(R.id.EtApellido);
        usuario = (EditText)findViewById(R.id.EtNombreUsuario);
        password = (EditText)findViewById(R.id.EtContraseña);
        email=(EditText)findViewById(R.id.EtEmail);
        sexo = (RadioGroup) findViewById(R.id.Rgsexo);
        CodArea=(EditText)findViewById(R.id.EtCodArea);
        Telefono = (EditText)findViewById(R.id.EtTelefono);
        nombrepersona =(TextInputLayout)findViewById(R.id.ImpNomUsuario);
        apellidopersona= (TextInputLayout)findViewById(R.id.ImpApellido);
        usuariomovil = (TextInputLayout)findViewById(R.id.ImpNombUsuarioApi);
        passmovil=(TextInputLayout)findViewById(R.id.ImpPass);
        emailmovil=(TextInputLayout)findViewById(R.id.Impemail);
        CodAreamovil=(TextInputLayout)findViewById(R.id.ImpCodA);
        Telefonomovil=(TextInputLayout)findViewById(R.id.Imptelf);

        SharedPreferences preferences = RegistroUsuarioFinal.this.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove("usuario");
        editor.remove("password");
        editor.commit();

        if (mayResquetStoragePermission()) {
            botonañadir.setEnabled(false);}
            else {
            botonañadir.setEnabled(true);}

        botonañadir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showOptions();
            }

            private void showOptions() {

                final CharSequence[] option = {"Tomar foto", "Elegir de galeria", "Cancelar"};
                final AlertDialog.Builder builder = new AlertDialog.Builder(RegistroUsuarioFinal.this);
                builder.setTitle("Elige una opción");
                builder.setItems(option, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(option[which] == "Tomar foto"){
                            openCamera();
                        }else if(option[which] == "Elegir de galeria"){
                            Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            intent.setType("image/*");
                            startActivityForResult(intent.createChooser(intent, "Selecciona app de imagen"), SELECT_PICTURE);
                        }else {
                            dialog.dismiss();
                        }
                    }
                });

                builder.show();

            }
        });


        Crear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //--Guarda Usuario
                SharedPreferences preferences = RegistroUsuarioFinal.this.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("usuario", usuario.getText().toString());
                editor.putString("password", password.getText().toString());
                editor.commit();



                // Validaciones de campo registro usuario
                Pattern NombreU = Pattern.compile("[a-zA-z ]*");
                Pattern ApellidoU = Pattern.compile("[a-zA-z ]*");
                Pattern NombUser = Pattern.compile("[a-z]*");
                Pattern PassU = Pattern.compile("[0-9]*");
                //No hace falta Pattern para el Email se usa el que te provee android.
                //Pattern FechaU = Pattern.compile("((0[1-9])|(1[0-9])|(2[0-9])|(3[0-1]))/((0[1-9])|(1[0-2]))/[0-9]{4}");
                Pattern CodAU = Pattern.compile("[0-9]*");
                Pattern TelfU = Pattern.compile("[0-9]*");

                if(NombreU.matcher(nombpers.getText().toString()).matches()==false){
                    nombrepersona.setError("Nombre de usuario  incorrecto");
                    nombreperapp=false;
                }else{
                    nombrepersona.setError(null);
                    nombreperapp=true;}

                if(ApellidoU.matcher(apepers.getText().toString()).matches()==false){
                    apellidopersona.setError("Apellido  incorrecto");
                    apelidoperapp=false;
                }else{
                    apellidopersona.setError(null);
                    apelidoperapp=true;}

                if(NombUser.matcher(usuario.getText().toString()).matches()==false){
                   usuariomovil.setError("Nombre de usuario  incorrecto");
                   usuarioapp=false;
                }else{
                    usuariomovil.setError(null);
                    usuarioapp=true;}

                if(PassU.matcher(password.getText().toString()).matches()==false){
                    passmovil.setError("Contraseña de usuario escrito incorrectamente");
                    passapp=false;
                }else{
                    passmovil.setError(null);
                    passapp=true;}

                if(Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()==false){
                    emailmovil.setError("Email escrito incorrectamente -> ej :user@gmail.com");
                    emailapp=false;
                }else{
                    emailmovil.setError(null);
                    emailapp=true;}

                //if(FechaU.matcher(Fecha.getText().toString()).matches()==false){
                    //Fechamovil.setError("Fecha escrita incorrectamente ->ej 29/03/1987");
                    //fechaapp=false;
                //}else{
                    //Fechamovil.setError(null);
                    //fechaapp=true;}

                if(CodAU.matcher(CodArea.getText().toString()).matches()==false){
                    CodAreamovil.setError("Nombre de usuario escrito incorrectamente");
                    codareaapp=false;
                }else{
                    CodAreamovil.setError(null);
                    codareaapp=true;}

                if(TelfU.matcher(Telefono.getText().toString()).matches()==false){
                   Telefonomovil.setError("Nombre de usuario escrito incorrectamente");
                   telefonoapp=false;
                }else{
                   Telefonomovil.setError(null);
                   telefonoapp=true;}

                // validadcion de que todos los Patterns pasaron correctamente.


                if(nombreperapp==true&& apelidoperapp==true && usuarioapp==true && passapp==true && emailapp==true  && codareaapp ==true && telefonoapp==true ){
                    AlertDialog.Builder builder = new AlertDialog.Builder(RegistroUsuarioFinal.this);
                    builder.setTitle("Confirmacion de Registo");
                    builder.setMessage("Usuario Creado Correctamente");
                    builder.setPositiveButton("Continuar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent Continuar = new Intent(RegistroUsuarioFinal.this, HomeUsuarioFinal.class);
                            startActivity(Continuar);
                        }
                    });
                    builder.show();

                }







            }

        });
        Cancelar = (Button)findViewById(R.id.BtnCancelar);
        Cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent Cancelar = new Intent(RegistroUsuarioFinal.this,TiposUsuarios.class);
                startActivity(Cancelar);
            }
        });
        Calendario =(Button)findViewById(R.id.BtnCalendar);
        Calendario.setOnClickListener(this);
        //..




    }
    @Override
    public void onClick(View view) {
        Fecha = (EditText)findViewById(R.id.EtFechaNac);
        if (view==Calendario){
            final Calendar Cale = Calendar.getInstance();
            Dia =Cale.get(Calendar.DAY_OF_MONTH);
            Mes= Cale.get(Calendar.MONTH);
            Año= Cale.get(Calendar.YEAR);

            DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                    Fecha.setText(i2+"/"+(i1+1)+"/"+i);
                }
            }
                    ,Dia,Mes,Año);
            datePickerDialog.show();
        }
    }
    private void openCamera() {
        File file = new File(Environment.getExternalStorageDirectory(), MEDIA_DIRECTORY);
        boolean isDirectoryCreated = file.exists();

        if(!isDirectoryCreated)
            isDirectoryCreated = file.mkdirs();

        if(isDirectoryCreated){
            Long timestamp = System.currentTimeMillis() / 1000;
            String imageName = timestamp.toString() + ".jpg";

            mPath = Environment.getExternalStorageDirectory() + File.separator + MEDIA_DIRECTORY
                    + File.separator + imageName;

            File newFile = new File(mPath);

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(newFile));
            startActivityForResult(intent, PHOTO_CODE);
        }
    }



    private boolean mayResquetStoragePermission() {
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
            return true;
        if((checkSelfPermission(WRITE_EXTERNAL_STORAGE)== PackageManager.PERMISSION_GRANTED)&&
                (checkSelfPermission(CAMERA)==PackageManager.PERMISSION_GRANTED))
            return  true;
        if((shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE))||(shouldShowRequestPermissionRationale(CAMERA))){
            Snackbar.make(scrolluser ,"Los permisos son necesarios para poder usar la aplicacion",Snackbar.LENGTH_INDEFINITE).setAction(android.R.string.ok, new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onClick(View view) {
                    requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE,CAMERA},MY_PERMISSIONS);
                }
            }).show();
        }else {
            requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE,CAMERA},MY_PERMISSIONS);
        }


        return false;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("file_path", mPath);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        mPath = savedInstanceState.getString("file_path");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode== RESULT_OK){
            switch (requestCode){
                case PHOTO_CODE:
                    MediaScannerConnection.scanFile(this,new String []{mPath},null, new OnScanCompletedListener() {
                        @Override
                        public void onScanCompleted(String path, Uri uri) {
                            Log.i("External Storage", "Scanned"+ path +":" );
                            Log.i("External Storage","->Uri ="+ uri);
                        }
                    });
                    Bitmap bitmap = BitmapFactory.decodeFile(mPath);
                    imagenfotouser.setImageBitmap(bitmap);
                    break;
                case SELECT_PICTURE:
                    Uri path = data.getData();
                    imagenfotouser.setImageURI(path);
                    break;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == MY_PERMISSIONS){
            if(grantResults.length == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED){
                Toast.makeText(RegistroUsuarioFinal.this, "Permisos aceptados", Toast.LENGTH_SHORT).show();
                botonañadir.setEnabled(true);
            }
        }else{
            showExplanation();
        }
    }

    private void showExplanation() {
        AlertDialog.Builder builder = new AlertDialog.Builder(RegistroUsuarioFinal.this);
        builder.setTitle("Permisos denegados");
        builder.setMessage("Para usar las funciones de la app necesitas aceptar los permisos");
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivity(intent);
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });

        builder.show();

    }
}
