package com.example.fiber.mievento;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.regex.Pattern;

public class Login extends AppCompatActivity {
    Button Registrar;
    Button Logueo;
    EditText Usuario;
    EditText Contraseña;
    Boolean Us;
    Boolean Cont;
    TextInputLayout ImpUs;
    TextInputLayout ImpCont;

    @Override
    public void onBackPressed() {}

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Registrar = (Button)findViewById(R.id.BtnRegistrar);
        Usuario= (EditText)findViewById(R.id.EtUsuario);
        Contraseña= (EditText)findViewById(R.id.EtContraseña);
        ImpUs= (TextInputLayout)findViewById(R.id.impUsuario);
        ImpCont=(TextInputLayout)findViewById(R.id.impContraseña);
        Registrar.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View view) {
                Intent Registrar = new Intent(Login.this ,TiposUsuarios.class);
                startActivity(Registrar);
            }
        });
        Logueo=(Button)findViewById(R.id.BtnLogin);

        //--Lee usuario
        SharedPreferences preferences = this.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
        final String usuarioString = preferences.getString("usuario", "");
        final String passwordString = preferences.getString("password", "");

        Logueo.setOnClickListener(new View.OnClickListener() {
           @Override
            public void onClick(View view) {

            Pattern userp = Pattern.compile("[a-z]*");

               if (!userp.matcher(Usuario.getText().toString()).matches()) {
                   ImpUs.setError("Usuario incorrecto ");
                   Us = false;

               }

               if(Usuario.getText().toString().equals(usuarioString)){
                    Us=true;
                    ImpUs.setError(null);
            }else{
                    ImpUs.setError("Usuario incorrecto ");
                    Us =false;

            }
               Pattern passp=  Pattern.compile("[0-9]*");
                if(!passp.matcher(Contraseña.getText().toString()).matches()){
                    ImpCont.setError("Contraseña Incorrecta");
                    Cont =false;

                }

                if(Contraseña.getText().toString().equals(passwordString)){
                        Cont=true;
                        ImpCont.setError(null);
                }else{
                        ImpCont.setError("Contraseña Incorrecta");
                        Cont =false;

                }

                if(Us==true && Cont==true){
                    Intent Logueo = new Intent(Login.this,HomeUsuarioFinal2.class);
                    startActivity(Logueo);

                }
            }
        });

    }


}
