package com.example.fiber.mievento;

import android.content.Intent;
import android.graphics.Typeface;
import android.renderscript.Script;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

public class TiposUsuarios extends AppCompatActivity {
    ImageButton PUsuario;
    //private TextView TvSTipoCuenta;
    //private TextView TvPUsuario;
    //private TextView TvPInstitucion;
    //private Typeface tromance;

    @Override
    public void onBackPressed() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipos_usuarios);

       // Tipo de Fuente
        //String fuente1 = "fuentes/tromance.ttf";
        //this.tromance = Typeface.createFromAsset(getAssets(),fuente1);
        //TvSTipoCuenta = (TextView) findViewById(R.id.TvSeleccionaTipoC);
        //TvSTipoCuenta.setTypeface(tromance);
        //TvPUsuario = (TextView) findViewById(R.id.TvPerfilUsuario);
        //TvPUsuario.setTypeface(tromance);
        //TvPInstitucion = (TextView) findViewById(R.id.TvPerfilInstitucion);
        //TvPInstitucion.setTypeface(tromance);

    //Pasar boton Perfil de usuario
    PUsuario= (ImageButton)findViewById(R.id.IbtnPerfilUsuario);
    PUsuario.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent Pusuario = new Intent(TiposUsuarios.this, RegistroUsuarioFinal.class);
            startActivity(Pusuario);

        }
    });


    }
}
