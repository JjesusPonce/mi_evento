package com.example.fiber.mievento;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

public class HomeUsuarioFinal extends AppCompatActivity {
    Button Continuar;
    CheckBox CbSaltar , CbCorrer, CbDriblear, CbNadar ,CbPelear , CbPatear , CbPesas , CbCarreras , CbAnotarP , CbAndarB ;
    @Override
    public void onBackPressed() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_usuario_final);
        Continuar= (Button) findViewById(R.id.BtnContinuar);
        Continuar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int i= Contar();
                if (i>=3){
                    Intent Continuar =new Intent(HomeUsuarioFinal.this , HomeUsuarioFinal2.class);
                    startActivity(Continuar);

                }

            }

            public int Contar() {
                CbSaltar = (CheckBox) findViewById(R.id.CbSaltar);
                CbCorrer = (CheckBox) findViewById(R.id.CbCorrer);
                CbDriblear = (CheckBox) findViewById(R.id.CbDriblear);
                CbNadar = (CheckBox) findViewById(R.id.CbNadar);
                CbPelear = (CheckBox) findViewById(R.id.CbPelear);
                CbPatear = (CheckBox) findViewById(R.id.CbPatear);
                CbPesas = (CheckBox) findViewById(R.id.CbPesas);
                CbCarreras = (CheckBox) findViewById(R.id.CbCarreras);
                CbAnotarP = (CheckBox) findViewById(R.id.CbAnotarPuntos);
                CbAndarB  = (CheckBox) findViewById(R.id.CbAndarBici);


                boolean A = (CbSaltar.isChecked());
                boolean B =(CbCorrer.isChecked());
                boolean C =(CbDriblear.isChecked());
                boolean D =(CbNadar.isChecked());
                boolean E =(CbPelear.isChecked());
                boolean F =(CbPatear.isChecked());
                boolean G =(CbPesas.isChecked());
                boolean H =(CbCarreras.isChecked());
                boolean I =(CbAnotarP.isChecked());
                boolean J =(CbAndarB.isChecked());
                int i=0;
                if (A == true) {
                    i = i + 1;
                    }
                if (B == true) {
                    i = i + 1;
                }
                if (C == true) {
                    i = i + 1;
                }
                if (D == true) {
                    i = i + 1;
                }
                if (E == true) {
                    i = i + 1;
                }
                if (F == true) {
                    i = i + 1;
                }
                if (G== true) {
                    i = i + 1;
                }
                if (H == true) {
                    i = i + 1;
                }
                if (I == true) {
                    i = i + 1;
                }
                if (J == true) {
                    i = i + 1;
                }

                return i;
            }
        });


    }


}


