package com.example.fiber.mievento;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by emilianochiozzi on 14/11/2017.
 */

public class EventoAdapter extends ArrayAdapter<JSONObject> {

    Context myContext;
    int myLayoutResourceID;
    ArrayList<JSONObject> myData = null;


    public EventoAdapter(Context context, int layoutResourceID,ArrayList<JSONObject> data) {
        super(context,layoutResourceID,data);

        this.myContext = context;
        this.myLayoutResourceID = layoutResourceID;
        this.myData = data;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View row = convertView;

        EventoHolder holder = null;

        if (row == null) {
            LayoutInflater inflater = ((Activity)myContext).getLayoutInflater();
            row = inflater.inflate(myLayoutResourceID,parent,false);
        }

        holder = new EventoHolder();
        holder.imagen = (ImageView)row.findViewById(R.id.imagenEvento);
        holder.nombre = (TextView)row.findViewById(R.id.nombreEvento);
        holder.precio = (TextView)row.findViewById(R.id.precioEvento);

        try {

            JSONObject evento = myData.get(position);
            holder.nombre.setText(evento.getString("nombre"));
            int imageId = myContext.getResources().getIdentifier("drawable/"+evento.getString("imagen"), null, myContext.getPackageName());
            holder.imagen.setImageResource(imageId);
            holder.precio.setText(String.valueOf(evento.getDouble("precio")));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return row;
    }

    static class EventoHolder {
        ImageView imagen;
        TextView nombre;
        TextView precio;
    }
}
