package com.example.fiber.mievento;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

public class HomeUsuarioFinal2 extends AppCompatActivity {

    public ArrayList<JSONObject> formList;

    ListView listaEventos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_usuario_final2);

        listaEventos = (ListView)findViewById(R.id.listaEventos);

        try {
            JSONObject obj = new JSONObject(loadJSONFromAsset());
            JSONArray m_jArry = obj.getJSONArray("eventos");
            formList = new ArrayList<JSONObject>();
            HashMap<String, String> m_li;

            for (int i = 0; i < m_jArry.length() - 1; i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);
                formList.add(jo_inside);
                Log.d("Nombre-->", String.valueOf(formList.get(i).getString("nombre")));
                Log.d("Precio-->", String.valueOf(formList.get(i).getDouble("precio")));

            }

            EventoAdapter adapter  = new EventoAdapter(this,R.layout.celda_evento,formList);

            listaEventos.setAdapter(adapter);

            listaEventos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    try {
                        Toast.makeText(getApplicationContext(),formList.get(i).getString("nombre"),Toast.LENGTH_SHORT).show();
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    }


                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = this.getAssets().open("eventos.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }


}
